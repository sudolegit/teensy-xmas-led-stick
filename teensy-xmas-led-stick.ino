////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@file
//!	
//!	@brief			Arduino sketch used to control WS2812B LED strips [NeoPixels] on Teensy USB 
//!					boards.
//!	
//!	@details		NeoPixels utilize thre wire communication:
//!						- Power [5V]
//!						- Ground
//!						- Data
//!	
//!					The NeoPixels should be powered separately from the Teensy with the data line 
//!					connected to Pin-## on the Teensy board. Control to LEDs is provided by 
//!					shifting out 24-bit values to form 8-bit R, G, and B values that each pixel 
//!					will utilize.
//!	
//!	@note			For details getting up to speed on the 'Adafruit_NeoPixel' library, please see:
//!						- https://learn.adafruit.com/adafruit-neopixel-uberguide/arduino-library-use
//!	
//!	@depends		
//!					- Teensy USB v3.2
//!					- Arduino v1.8.5++
//!					- Designed around ALITOVE WS2812B LED strips that are configured as:
//!						- GRB color order.
//!						- 400 KHz
//!	
//!	@author			Michael Stephens
////////////////////////////////////////////////////////////////////////////////////////////////////


//==================================================================================================
// LOAD IN EXTERNAL DEPENDENCIES
//--------------------------------------------------------------------------------------------------
#include <EEPROM.h>																// Provides access to EEProm.
#include <Adafruit_NeoPixel.h>													// Provides access to NeoPixel library provided by Adafruit.




//==================================================================================================
// DEFINE CONSTANTS USES IN FILE
//--------------------------------------------------------------------------------------------------
// Define abstractions to physical pins on the board.
#define	PIN_NEOPIXEL_DATA				0										//!< GPIO pin to use when communicating to the NeoPixel strip.
#define PIN_BUTTON_PATTERN				1										//!< Interrupt pin associated with the input button used to change active patterns.
#define PIN_BUTTON_MODE					2										//!< Interrupt pin associated with the input button used to change active patterns.
#define PIN_BUTTON_DELAY				3										//!< Interrupt pin associated with the input button used to change active patterns.

#define	SERIAL_BAUD_RATE				9600									//!< Data rate used when printing messages to the serial port.

// Define NeoPixel related values.
#define NEOPIXEL_NUMB_LEDS				30										//!< Number of LEDs on strip that will be controlled.
#define	NEOPIXEL_FLAGS					(NEO_GRB + NEO_KHZ400)					//!< Flags used in 'Adafruit_NeoPixel{}' instance. LED color order + data frequency.

#define	NEOPIXEL_BRIGHTNESS_LEVEL		255										//!< Maximum output level per color allowed (essentially a brighness limiter).
#define	BRIGHTNESS_FADER_DELTA			50										//!< Number of steps to adjust by when fading brightness levels.

////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@brief			Define LED patterns supported by system as well as the order they are cycled in.
//!	
//!	@note			To effectively remove / ignore patterns from looping list executed while 
//!					pressing buttons, move the location of '__PATTERN_MAX' (don't delete entries, 
//!					but will only loop from start of enum to point of '__PATTERN_MAX').
////////////////////////////////////////////////////////////////////////////////////////////////////
enum PATTERN
{
	PATTERN_RED,																//!< All LEDs are red.
	PATTERN_GREEN,																//!< All LEDs are green.
	PATTERN_BLUE,																//!< All LEDs are blue.
	PATTERN_GREEN_AND_RED,														//!< LEDs alternate between green and red (starting with green).
	PATTERN_CANDY_CANE,															//!< LEDs alternate between red and white (starting with red).
	__PATTERN_MAX,																//!< Placeholder enter to indicate maximum pattern value presently supported.
	PATTERN_WHITE																//!< All LEDs are white.
};

#define	DEFAULT_PATTERN					PATTERN_GREEN_AND_RED					//!< Default pattern used on system at boot.

////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@brief			Define operation modes supported by system. A mode defines if LEDs are static
//!					or transitioning in any way.
//!	
//!	@note			To effectively remove / ignore modes from looping list executed while 
//!					pressing buttons, move the location of '__MODE_MAX' (don't delete entries, 
//!					but will only loop from start of enum to point of '__MODE_MAX').
////////////////////////////////////////////////////////////////////////////////////////////////////
enum MODE
{
	MODE_ON,																	//!< All LEDs are on with no transitions active.
	MODE_BLINK_EVENS,															//!< Every other LED blinks starting with the second LED.
	MODE_BLINK_ODDS,															//!< Every other LED blinks starting with the first LED.
	MODE_BLINK_ALL,																//!< All LEDs blink.
	MODE_ROR,																	//!< Rotate LEDs in a circle clockwise.
	MODE_FADER,																	//!< Fade all LEDs in and out.
	__MODE_MAX,																	//!< Placeholder enter to indicate maximum mode value presently supported.
	MODE_ROL																	//!< Rotate LEDs in a circle counter-clockwise.
};

#define	DEFAULT_MODE					MODE_BLINK_EVENS						//!< Default mode used on system at boot.

////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@brief				Defines delay values that will be cycled through by pressing the delay 
//!						button.
//!	
//!	@warning			If you wish to change the options presented to the end-user, you must 
//!						modify the logic in 'InterruptButtonPressDelay()' (not dynamic like pattern 
//!						or mode; we would need to not treat the delays as actual values to support 
//!						dynamic range).
////////////////////////////////////////////////////////////////////////////////////////////////////
enum DELAYS
{
	DELAY_10							= 10,									//< Delay for 10 milliseconds.
	DELAY_100							= 100,									//< Delay for 100 milliseconds.
	DELAY_250							= 250,									//< Delay for 250 milliseconds.
	DELAY_500							= 500,									//< Delay for 500 milliseconds.
	DELAY_1000							= 1000,									//< Delay for 1   second.
    __DELAY_MAX																	//!< Placeholder enter to indicate maximum mode value presently supported.
};

#define	DEFAULT_DELAY_MILLI_SECS		DELAY_1000								//!< Default dealy used between loop iterations (in milliseconds).
#define	DELAY_SYSTEM_BOOT				500										//!< Delay at boot before processing begins. Serves as a safety measure for system to reach a steady state.

// Define EEPROM related constants.
#define	EEPROM_MAX_ADDR_OFFSET			0										//!< Offset from 0 for tarting memory address allowed to read/write from/to. Used within EEPROM...() methods so user does not need to know/care of shifting start address if EEPROM region degrades.
#define	EEPROM_MAX_RELATIVE_ADDR		100										//!< Ending memory address allowed to read/write from/to relative to EEPROM_MAX_ADDR_OFFSET.
#define	EEPROM_MAX_VAR_BYTE_COUNT		4										//!< Maximum allowed length (in bytes) for writing to EEPROM.
#define	EEPROM_ADDR_PATTERN				0										//!< Address value to use for EEPROM reads and writes related to active pattern.
#define	EEPROM_ADDR_MODE				1										//!< Address value to use for EEPROM reads and writes related to active mode.
#define	EEPROM_ADDR_DELAY_BYTE_0		2										//!< Address value to use for EEPROM reads and writes related to delay settings.

#define	DEBOUNCE_MIN_TIME_DELTA_MS		250										//!< Minimum time between interrupts allowed to be considered a valid button press.




//==================================================================================================
// GLOBAL VARIABLE(S)
//--------------------------------------------------------------------------------------------------
int64_t					m_count;												//!< Global counter tracking how many iterations have occurred thus far.
uint32_t				m_delay;												//!< Time to delay between loop iterations.
unsigned long			m_lastLoopTimeMs;										//!< Used to track the time (milliseconds) of the last loop instance. Utilized to run rough delays [rather than an actual delay which would break interrupt logic].
unsigned long			m_lastInterruptTimeMs;									//!< Used to track the time (milliseconds) of the last interrupt press. Utilized in debouncing of HW interrupts.
Adafruit_NeoPixel		m_strip;												//!< Instance providing helper methods and definitions for controlling LED output for NeoPixels.
uint32_t				m_pixelsPrev[NEOPIXEL_NUMB_LEDS];						//!< Instance pixel values at end of main 'loop()'. Referenced during modes.
PATTERN					m_pattern;												//!< Active pattern on system.
MODE					m_mode;													//!< Active mode on system.
int16_t					m_faderDelta;											//!< Tracks the current amount and direction the 'MODE_FADER' will adjust the current brightness level by when it is next invoked.




//==================================================================================================
//	CORE ARDUINO FUNCTION IMPLEMENTATIONS [FUNCTIONS WITH SPECIAL MEANING FOR ARDUINO SKETCHES]
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@brief			Initialization function for platform. Runs once at boot.
////////////////////////////////////////////////////////////////////////////////////////////////////
void setup()
{
	// Delay system boot for safety.
	delay(DELAY_SYSTEM_BOOT);
	
	// Initialize serial port so that we can publish change notifications to it.
	Serial.begin(SERIAL_BAUD_RATE);
	
	// Ensure tracking variables and counters are properly defined.
	m_count			= 0;														// Set tracking counter to zero.
	m_faderDelta	= -BRIGHTNESS_FADER_DELTA;									// Ensure brightness direction begins by dimming.
	
	// Ensure the data line we are using is configured as an output pin.
	pinMode(PIN_NEOPIXEL_DATA, OUTPUT);
	
	// Ensure the interrupt pins for push buttons are input lines.
	pinMode(PIN_BUTTON_PATTERN,	INPUT_PULLUP);
	pinMode(PIN_BUTTON_MODE,	INPUT_PULLUP);
	pinMode(PIN_BUTTON_DELAY,	INPUT_PULLUP);
	
	// Configure hardware interrupt callbacks.
	attachInterrupt(PIN_BUTTON_PATTERN,	InterruptButtonPressPattern,	FALLING);
	attachInterrupt(PIN_BUTTON_MODE,	InterruptButtonPressMode,		FALLING);
	attachInterrupt(PIN_BUTTON_DELAY,	InterruptButtonPressDelay,		FALLING);
	
	// Initialize global instance of LED strip.
	// Parameter 1 = number of pixels in strip
	// Parameter 2 = pin number (most are valid)
	// Parameter 3 = pixel type flags, add together as needed:
	//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
	//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
	//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
	//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
	m_strip = Adafruit_NeoPixel(NEOPIXEL_NUMB_LEDS, PIN_NEOPIXEL_DATA, NEOPIXEL_FLAGS);
	
	// Push configuration out to strip.
	m_strip.begin();
	
	// Determine default values for global variables tracked in EEPROM.
	EEPROMLoadDefaults();
	
	// Set initial pattern.
	PatternActivate(m_pattern);
	
	// Apply default brightness level.
	m_strip.setBrightness(NEOPIXEL_BRIGHTNESS_LEVEL);
	
	// Push out all current settings to NeoPixels.
	m_strip.show();
	
	// [PATCH] Delay as if one loop has been completed. Needed for patterns to avoid awkward flash 
	// at start of boot. Could also handle by changing delay location in loop, but prefer this 
	// method.
	delay(m_delay);
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@brief			Main processing loop.
////////////////////////////////////////////////////////////////////////////////////////////////////
void loop()
{
	unsigned long	timeMs				= millis();								// Current time in milliseconds.
	
	// Proceed with processing so long as there has been a long enough delay to match the global 
	// delay request.
	if( abs(timeMs - m_lastLoopTimeMs) >= m_delay )
	{
		// Apply the current mode onto the existing pattern.
		ModeApply(m_mode);
		
		// Push out current pattern.
		m_strip.show();
		
		// Update rolling counter.
		++m_count;
		
		// Save processing time to use on next effectively delay.
		m_lastLoopTimeMs = timeMs;
	}
}




//==================================================================================================
//	FUNCTIONS :: LED PATTERNS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Function invoked each time the 'PIN_BUTTON_PATTERN' pin transitions from high to 
//!					low. When invoked, debounces input and cycles through available patterns.
//!	
//!	@details		Polls the current processor time in milliseconds and ensures that at least 
//!					'DEBOUNCE_MIN_TIME_DELTA_MS' has passed since any external button was pressed.
//!					Determines the next pattern in the enum 'PATTERN' and activates it. 
//!					Additionally, the EEPROM is updated to save the requested value.
////////////////////////////////////////////////////////////////////////////////////////////////////
void InterruptButtonPressPattern()
{
	unsigned long	timeMs				= millis();								// Current time in milliseconds.
	
	noInterrupts();
	
	// Proceed with processing so long as there has been a long enough delay since the last 
	// triggering of this function.
	if( abs(timeMs - m_lastInterruptTimeMs) > DEBOUNCE_MIN_TIME_DELTA_MS )
	{
		// Apply next pattern in list and save the pattern values to ensure the active mode behaves 
		// properly (does not fail to show presently disabled LEDs).
		PatternActivate( (PATTERN)(((uint16_t)m_pattern + 1) % (uint16_t)__PATTERN_MAX) );
		PatternSave();
		
		// Update saved variable in EEPROM with latest pattern.
		EEPROMSaveIfModified(EEPROM_ADDR_PATTERN, (uint8_t *)&m_pattern, 1, false);
		
		m_lastInterruptTimeMs = timeMs;
	}
	
	interrupts();
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Function invoked each time the 'PIN_BUTTON_MODE' pin transitions from high to 
//!					low. When invoked, debounces input and cycles through available modes.
//!	
//!	@details		Polls the current processor time in milliseconds and ensures that at least 
//!					'DEBOUNCE_MIN_TIME_DELTA_MS' has passed since any external button was pressed.
//!					Determines the next mode in the enum 'MODE' and activates it. Additionally, 
//!					the EEPROM is updated to save the requested value.
//!	
//!	@note			This method handles updating the brightness level of all NeoPixels when not 
//!					transitioning to a 'fader' mode so that all LEDs are at the default brightness 
//!					level.
////////////////////////////////////////////////////////////////////////////////////////////////////
void InterruptButtonPressMode()
{
	unsigned long	timeMs				= millis();								// Current time in milliseconds.
	
	noInterrupts();
	
	// Proceed with processing so long as there has been a long enough delay since the last 
	// triggering of this function.
	if( abs(timeMs - m_lastInterruptTimeMs) > DEBOUNCE_MIN_TIME_DELTA_MS )
	{
		// No need to apply the mode presently as happens every loop, but we should update the 
		// value that is used to apply the mode.
		m_mode = (MODE)( ((uint16_t)m_mode + 1) % (uint16_t)__MODE_MAX );
		
		// Forcibly reload the existing pattern to prevent quirks.
		PatternActivate(m_pattern);
		PatternSave();
		
		// Update saved variable in EEPROM with latest mode.
		EEPROMSaveIfModified(EEPROM_ADDR_MODE, (uint8_t *)&m_mode, 1, false);
		
		// Handle transition mode events.
		if( m_mode != MODE_FADER)
			// Force brightness to default value when switching modes to avoid issues with the fader 
			// mode leaving the LEDs too dim.
			m_strip.setBrightness(NEOPIXEL_BRIGHTNESS_LEVEL);
		else
			// Ensure brightness direction begins by dimming.
			m_faderDelta = -BRIGHTNESS_FADER_DELTA;
		
		m_lastInterruptTimeMs = timeMs;
	}
	
	interrupts();
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Function invoked each time the 'PIN_BUTTON_DELAY' pin transitions from high to 
//!					low. When invoked, debounces input and cycles through available delays.
//!	
//!	@details		Polls the current processor time in milliseconds and ensures that at least 
//!					'DEBOUNCE_MIN_TIME_DELTA_MS' has passed since any external button was pressed.
//!					Determines the next delay in the enum 'DELAYS' and activates it. Additionally, 
//!					the EEPROM is updated to save the requested value.
////////////////////////////////////////////////////////////////////////////////////////////////////
void InterruptButtonPressDelay()
{
	unsigned long	timeMs				= millis();								// Current time in milliseconds.
	
	noInterrupts();
	
	// Proceed with processing so long as there has been a long enough delay since the last 
	// triggering of this function.
	if( abs(timeMs - m_lastInterruptTimeMs) > DEBOUNCE_MIN_TIME_DELTA_MS )
	{
		if( m_delay == DELAY_10 )
			m_delay = DELAY_100;
		else if( m_delay == DELAY_100 )
			m_delay = DELAY_250;
		else if( m_delay == DELAY_250 )
			m_delay = DELAY_500;
		else if( m_delay == DELAY_500 )
			m_delay = DELAY_1000;
		else if( m_delay == DELAY_1000 )
			m_delay = DELAY_10;
		else
			m_delay = DEFAULT_DELAY_MILLI_SECS;
		
		// Update saved variable in EEPROM with latest delay.
		EEPROMSaveIfModified(EEPROM_ADDR_DELAY_BYTE_0, (uint8_t *)&m_delay, 4, false);
		
		m_lastInterruptTimeMs = timeMs;
	}
	
	interrupts();
}




//==================================================================================================
//	FUNCTIONS :: LED PATTERNS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Read EEPROM for various settings saved to memory. When values read are invalid, 
//!					enforce default values.
////////////////////////////////////////////////////////////////////////////////////////////////////
void EEPROMLoadDefaults()
{
	// Load in the default 'pattern' value saved in EEPROM. If value is invalid, enforce default.
	EEPROMReadValues(EEPROM_ADDR_PATTERN, (uint8_t *)&m_pattern, 1);
	if (m_pattern >= __PATTERN_MAX)
		m_pattern = DEFAULT_PATTERN;
	Serial.print("Initial pattern set to:\t");
	Serial.println(m_pattern);
	
	// Load in the default 'mode' value saved in EEPROM. If value is invalid, enforce default.
	EEPROMReadValues(EEPROM_ADDR_MODE, (uint8_t *)&m_mode, 1);
	if (m_mode >= __MODE_MAX)
		m_mode = DEFAULT_MODE;
	Serial.print("Initial mode set to:\t");
	Serial.println(m_mode);
	
	// Load in the default 'delay' value saved in EEPROM. If value is invalid, enforce default.
	// Note:	No good way to check a limit on dealy time without looping so just ensuring value 
	//			read is non-zero.
	EEPROMReadValues(EEPROM_ADDR_DELAY_BYTE_0, (uint8_t *)&m_delay, 4);
	if (m_delay > (int32_t)__DELAY_MAX)
		m_delay = DEFAULT_DELAY_MILLI_SECS;
	Serial.print("Initial delay set to:\t");
	Serial.println(m_delay);
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@brief			Read values from EEPROM.
//!	
//!	@note			Values are read in big endian format.
//!	
//!	@param[in]		addr				EEPROM address to begin reading from.
//!	@param[out]		val					Value to read from EEEPROM.
//!	@param[in]		len					Length of value to read from EEPROM.
//!	
//!	@retval			0					Successfully pushed values to EEPROM.
//!	@retval			1					Requested address is invalid.
//!	@retval			2					Requested length is invalid.
////////////////////////////////////////////////////////////////////////////////////////////////////
int32_t	EEPROMReadValues(int addr, uint8_t *val, uint8_t len)
{
	int				i						= 0;								// Counter used in loops.
	
	// Exit if length or address is invalid.
	if( (addr+EEPROM_MAX_ADDR_OFFSET) < EEPROM_MAX_ADDR_OFFSET || (addr+len) > EEPROM_MAX_RELATIVE_ADDR)
	{
		return 1;
	}
	else if( len == 0 || len > EEPROM_MAX_VAR_BYTE_COUNT)
	{
		return 2;
	}
	
	// Account for offset in address.
	addr += EEPROM_MAX_ADDR_OFFSET;
	
	// Execute read.
	for(i=(len-1); i >= 0; --i)
	{
		val[i] = EEPROM.read(addr+i);
	}
	
	return 0;
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@brief			Save provided values to EEPROM so long as value is new.
//!	
//!	@note			We do not presently handle shifts in where we write to EEPROM to try to protect 
//!					memory corruption and delay product life. However, we do avoid writing the same 
//!					value back to EEPROM that is already there by first reading EEPROM.
//!	
//!	@note			Values are pushed in big endian format.
//!	
//!	@param[in]		addr				EEPROM address to begin reading from.
//!	@param[in]		val					Value to write to EEEPROM.
//!	@param[in]		len					Length of value to write to EEPROM.
//!	@param[in]		validate			Flag indicating if the value pushed should be validated 
//!										(read flash to confirm value pushed).
//!	
//!	@retval			0					Successfully pushed values to EEPROM.
//!	@retval			1					Requested address is invalid.
//!	@retval			2					Requested length is invalid.
//!	@retval			3					No need to write to flash as value already matches.
//!	@retval			4					Flash written to but validation step failed.
////////////////////////////////////////////////////////////////////////////////////////////////////
int32_t EEPROMSaveIfModified(int addr, uint8_t *val, uint8_t len, bool validate)
{
	uint8_t			valCurrent[EEPROM_MAX_VAR_BYTE_COUNT];						// Stores value read in from EEPROM.
	int				i						= 0;								// Counter used in loops.
	
	// Notify user of request.
	Serial.print("Requesting push of ");
	Serial.print(len, DEC);
	Serial.print("-byte value '0x");
	for(i=(len-1); i >= 0; --i)
	{
		// Zero-pad bytes that do not take up two nibbles in length.
		if( *(val+i) < 0x0F )
			Serial.print('0');
		Serial.print(*(val+i), HEX);
	}
	Serial.print("' to EEPROM starting at address '");
	Serial.print(addr);
	Serial.print("' ... ");
	
	// Exit if length or address is invalid.
	if( (addr+EEPROM_MAX_ADDR_OFFSET) < EEPROM_MAX_ADDR_OFFSET || (addr+len) > EEPROM_MAX_RELATIVE_ADDR)
	{
		Serial.print("ABORTING. Address value of '");
		Serial.print(len);
		Serial.println("' is invalid");
		return 1;
	}
	else if( len == 0 || len > EEPROM_MAX_VAR_BYTE_COUNT)
	{
		Serial.print("ABORTING. Length of value '");
		Serial.print(len);
		Serial.println("' is invalid");
		return 2;
	}
	
	// Account for offset in address.
	addr += EEPROM_MAX_ADDR_OFFSET;
	
	// Read in current value from EEPROM.
	EEPROMReadValues(addr, &valCurrent[0], len);
	
	// Write to EEPROM if value is unique.
	if( memcmp(val, &valCurrent[0], len) != 0 )
	{
		Serial.print("writing to EEPROM ... ");
		for(i=(len-1); i >= 0; --i)
		{
			EEPROM.write((addr + i), *(val + i));
		}
	}
	else
	{
		Serial.println("ABORTING. Value provided matches current values.");
		return 3;
	}
	
	// Validate flash if requested to do so.
	if(validate == true)
	{
		// Read in current value from EEPROM.
		EEPROMReadValues(addr, &valCurrent[0], len);
		
		// Run validation check.
		if( memcmp(val, &valCurrent[0], len) != 0 )
		{
			Serial.println("validation FAILED ... DONE.");
			return 4;
		}
		else
		{
			Serial.println("validation PASSED ... DONE.");
		}
	}
	else
	{
		Serial.println("skipping validation check ... DONE.");
	}
	
	return 0;
	
}



//==================================================================================================
//	FUNCTIONS :: LED PATTERNS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Apply [activate] the pattern for the attached LED strip.
//!	
//!	@details		This is the main function which defines LED colors for a pattern [by invoking 
//!					the relevat function]. It does not handle pattern changes, but rather applies 
//!					the base pattern desired.
////////////////////////////////////////////////////////////////////////////////////////////////////
void PatternActivate(PATTERN ptn)
{
	// Store current pattern value for reference later.
	m_pattern = ptn;
	
	// Activate the LED pattern requested.
	switch(ptn)
	{
		case PATTERN_RED:
			PatternRed();
			break;
		case PATTERN_GREEN:
			PatternGreen();
			break;
		case PATTERN_BLUE:
			PatternBlue();
			break;
		case PATTERN_WHITE:
			PatternWhite();
			break;
		case PATTERN_GREEN_AND_RED:
			PatternGreenAndRed();
			break;
		case PATTERN_CANDY_CANE:
			PatternCandyCane();
			break;
		case __PATTERN_MAX:
			// Do nothing. Including here to suppress compiler warnings.
			break;
	}
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Saves a copy of the RGB [pixel] values for all LEDs as they are presently 
//!					defined.
//!	
//!	@details		Updates the global 'm_pixeslPrev' array with the contents of each pixel value 
//!					within the 'm_strip' instance. This allows a smaller copy than would be needed 
//!					with a second instance and avoids issues with 'getPixels()' returning a pointer 
//!					with an unknown length due to variable pixel data length based on hardware 
//!					requirements [determined during instantiation and not exposed to this level].
////////////////////////////////////////////////////////////////////////////////////////////////////
void PatternSave()
{
	for(int i=0; i < NEOPIXEL_NUMB_LEDS; ++i)
	{
		m_pixelsPrev[i] = m_strip.getPixelColor(i);
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Loops throug the current 'm_pixelsPrev' array and applies all values to the 
//!					'm_strip' instance. Effectively, this restores a saved pattern.
////////////////////////////////////////////////////////////////////////////////////////////////////
void PatternRestore()
{
	for(int i=0; i < NEOPIXEL_NUMB_LEDS; ++i)
	{
		m_strip.setPixelColor(i, m_pixelsPrev[i]);
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Turn off all LEDs.
////////////////////////////////////////////////////////////////////////////////////////////////////
void PatternNone()
{
	for(int i=0; i < NEOPIXEL_NUMB_LEDS; ++i)
	{
        m_strip.setPixelColor(i, 0, 0, 0);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Set all LEDs on NeoPixel string to be red.
////////////////////////////////////////////////////////////////////////////////////////////////////
void PatternRed()
{
	for(int i=0; i < NEOPIXEL_NUMB_LEDS; ++i)
	{
        m_strip.setPixelColor(i, 255, 0, 0);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Set all LEDs on NeoPixel string to be green.
////////////////////////////////////////////////////////////////////////////////////////////////////
void PatternGreen()
{
	for(int i=0; i < NEOPIXEL_NUMB_LEDS; ++i)
	{
        m_strip.setPixelColor(i, 0, 255, 0);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Set all LEDs on NeoPixel string to be blue.
////////////////////////////////////////////////////////////////////////////////////////////////////
void PatternBlue()
{
	for(int i=0; i < NEOPIXEL_NUMB_LEDS; ++i)
	{
        m_strip.setPixelColor(i, 0, 0, 255);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Set all LEDs on NeoPixel string to be white.
////////////////////////////////////////////////////////////////////////////////////////////////////
void PatternWhite()
{
	for(int i=0; i < NEOPIXEL_NUMB_LEDS; ++i)
	{
        m_strip.setPixelColor(i, 255, 255, 255);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Alternate between green and red leds starting with green.
////////////////////////////////////////////////////////////////////////////////////////////////////
void PatternGreenAndRed()
{
	for(int i=0; i < NEOPIXEL_NUMB_LEDS; ++i)
	{
		if( (i % 2) == 0)
			m_strip.setPixelColor(i, 0, 255, 0);
		else
			m_strip.setPixelColor(i, 255, 0, 0);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Alternamte between red and white leds starting with red.
//!	
//!	@note			Intended to invoke the sense of a candy cane.
////////////////////////////////////////////////////////////////////////////////////////////////////
void PatternCandyCane()
{
	for(int i=0; i < NEOPIXEL_NUMB_LEDS; ++i)
	{
		if( (i % 2) == 0)
			m_strip.setPixelColor(i, 255, 0, 0);
		else
			m_strip.setPixelColor(i, 255, 255, 255);
    }
}




//==================================================================================================
//	FUNCTIONS :: EFFECTS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Apply the requested mode on top of the active pattern for the attached LED 
//!					strip.
//!	
//!	@details		This is the main function which applies modes to modify the active pattern. It 
//!					does not directly manage saving of existing patter (should be handled within a 
//!					mode function).
////////////////////////////////////////////////////////////////////////////////////////////////////
void ModeApply(MODE mo)
{
	// Apply the mode requested on the active pattern.
	switch(mo)
	{
		case MODE_ON:
			// Do nothing [leave pattern 'as is'].
			break;
		case MODE_BLINK_ALL:
			ModeBlinkAll();
			break;
		case MODE_BLINK_EVENS:
			ModeBlinkEvens();
			break;
		case MODE_BLINK_ODDS:
			ModeBlinkOdds();
			break;
		case MODE_ROR:
			ModeROR();
			break;
		case MODE_ROL:
			ModeROL();
			break;
		case MODE_FADER:
			ModeFader();
			break;
		case __MODE_MAX:
			// Do nothing. Including here to suppress compiler warnings.
			break;
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Alternate flashing all LEDs on and off.
////////////////////////////////////////////////////////////////////////////////////////////////////
void ModeBlinkAll()
{
	if( (m_count % 2) == 0 )
	{
		PatternSave();
		PatternNone();
	}
	else
	{
		PatternRestore();
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Blinks every other LED starting at 2 (e.g. 2, 4, 6, ...).
////////////////////////////////////////////////////////////////////////////////////////////////////
void ModeBlinkEvens()
{
	if( (m_count % 2) == 0 )
	{
		PatternSave();
		for(int i=1; i < NEOPIXEL_NUMB_LEDS; i+=2)
		{
			m_strip.setPixelColor(i, 0, 0, 0);
		}
	}
	else
	{
		PatternRestore();
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Blinks every other LED starting at 1 (e.g. 1, 3, 5, ...).
////////////////////////////////////////////////////////////////////////////////////////////////////
void ModeBlinkOdds()
{
	if( (m_count % 2) == 0 )
	{
		PatternSave();
		for(int i=0; i < NEOPIXEL_NUMB_LEDS; i+=2)
		{
			m_strip.setPixelColor(i, 0, 0, 0);
		}
	}
	else
	{
		PatternRestore();
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Shifts LEDs left (rolling over the rightmost LED to the leftmost). Effectively, 
//!					rotates LEDs clockwise.
////////////////////////////////////////////////////////////////////////////////////////////////////
void ModeROR()
{
	int				i					= 1;
	
	// Save pattern before shifting to make it easier to avoid eating our own tail.
	PatternSave();
	
	// Loop through starting at the second LED index and shift all values right.
	for(; i < NEOPIXEL_NUMB_LEDS; ++i)
	{
		m_strip.setPixelColor(i, m_pixelsPrev[i-1]);
	}
	
	// Copy over the original 'last value' to the first entry in the LED list.
	m_strip.setPixelColor(0, m_pixelsPrev[i-1]);
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Shifts LEDs left (rolling over the leftmost LED to the rightmost). Effectively, 
//!					rotates LEDs counter-clockwise.
////////////////////////////////////////////////////////////////////////////////////////////////////
void ModeROL()
{
	int				i					= (NEOPIXEL_NUMB_LEDS - 1);
	
	// Save pattern before shifting to make it easier to avoid eating our own tail.
	PatternSave();
	
	// Loop through starting at the last LED index and shift all values left.
	for(; i > 0; --i)
	{
		m_strip.setPixelColor((i-1), m_pixelsPrev[i]);
	}
	
	// Copy over the original 'first value' to the last entry in the LED list.
	m_strip.setPixelColor((NEOPIXEL_NUMB_LEDS - 1), m_pixelsPrev[0]);
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief          Loops through all fader levels and continually modulates brightness levels of 
//!					LEDs. Fades from bright to zero and back to bright again in a continuous loop.
//!	
//!	@note			Brightness will increase or decrease by the global constant 
//!					'BRIGHTNESS_FADER_DELTA'.
////////////////////////////////////////////////////////////////////////////////////////////////////
void ModeFader()
{
	int16_t			lvl					= (int16_t)m_strip.getBrightness();		// Current brightness level for LEDs.
	
	// Detect rollover from min to max value.
	if( lvl + m_faderDelta < 0 )
		m_faderDelta = BRIGHTNESS_FADER_DELTA;
	// Detect rollover from max to min value.
	else if( lvl + m_faderDelta > 255 )
		m_faderDelta = -BRIGHTNESS_FADER_DELTA;
	
	m_strip.setBrightness((uint8_t)(lvl + m_faderDelta));
	
}


